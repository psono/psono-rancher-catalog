# Readme


## Overview

#### Psono server

The Psono Server stack has eight components:

- Load Balancer: Distribute the load between multiple instances and exposes the main port.
- Psono Server: The "core" of the system, doing all the logic
- Psono Worker: Runs background tasks
- Master Database: Postgres master database to store all data and target for write operations
- Slave Database: Postgres slave database as target for read operations
- Redis: Shared Redis cache between Psono server instances
- Mail Server: To send mails e.g. for the registration process

A Graph looks like this

![Psono Server](images/psono-server-graph.png)


#### Psono client

The Psono Client stack has only two component:

- Load Balancer: Distribute the load between multiple instances and exposes the main port.
- Psono Client: Simple webserver hosting the Psono Client

A Graph looks like this

![Psono Client](images/psono-client-graph.png)

## Installation

We assume that you already have a rancher server running. If not follow this guide:
https://docs.rancher.com/rancher/v1.5/en/installing-rancher/installing-server/

or in short:

1) Install Docker
2) Execute:

        sudo docker run -d -v /opt/docker/rancher-server:/var/lib/mysql --restart=unless-stopped -p 8080:8080 rancher/server

### Add New Catalog

1) Go to "Admin -> Settings"
2) Add new Catalog with this URL: https://gitlab.com/psono/psono-rancher-catalog.git
    
### Start new PSONO Server Stack

1) Go to "Catalog -> 'Catalog name' -> PSONO Server View Details"
2) Provide a random `DB Password` and `Replication Password` (backup those)
3) Create the directories for `Master DB data dir` an so on, e.g.:

	mkdir -p /opt/docker/production-psono-db-master/dbdata
	mkdir -p /opt/docker/production-psono-db-slave/dbdata
	mkdir -p /opt/docker/production-psono-mail/mail
	mkdir -p /opt/docker/production-psono-mail/config
         
4) Create a settings.yaml and specify the path to it, e.g.:

	/opt/docker/production-psono-server/settings.yaml
	
It should look similar to this one:
    
	# generate the following six parameters with the following command
	# docker run --rm -ti psono/psono-server:latest psono/manage.py generateserverkeys
	SECRET_KEY: 'SOME SUPER SECRET KEY THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
	ACTIVATION_LINK_SECRET: 'SOME SUPER SECRET ACTIVATION LINK SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
	DB_SECRET: 'SOME SUPER SECRET DB SECRET THAT SHOULD BE RANDOM AND 32 OR MORE DIGITS LONG'
	EMAIL_SECRET_SALT: '$2b$12$XUG.sKxC2jmkUvWQjg53.e'
	PRIVATE_KEY: '302650c3c82f7111c2e8ceb660d32173cdc8c3d7717f1d4f982aad5234648fcb'
	PUBLIC_KEY: '02da2ad857321d701d754a7e60d0a147cdbc400ff4465e1f57bc2d9fbfeddf0b'
		
	# The URL of the web client (path to e.g activate.html without the trailing slash)
	# WEB_CLIENT_URL: 'https://www.psono.pw'
		
	# Switch DEBUG to false if you go into production
	DEBUG: False
		
	# Adjust this according to Django Documentation https://docs.djangoproject.com/en/1.10/ref/settings/
	ALLOWED_HOSTS: ['*']
		
	# Should be your domain without "www.". Will be the last part of the username
	ALLOWED_DOMAINS: ['psono.pw']
	
	# Switch DEBUG to false if you go into production
    DEBUG: False
    
    # Adjust this according to Django Documentation https://docs.djangoproject.com/en/1.10/ref/settings/
    ALLOWED_HOSTS: ['*']
    
    # Should be your domain without "www."
    # If you dont care about user data export (not implemented yet), you can put anything here that looks like a domain
    ALLOWED_DOMAINS: ['example.com']
	
	# Should be the URL of the host under which the host is reachable
    # If you open the url you should have a text similar to {"detail":"Authentication credentials were not provided."}
    HOST_URL: 'http://example.com/server'
	
	EMAIL_FROM: 'info@example.com'
	EMAIL_HOST: 'mail'
	EMAIL_HOST_USER: 'system@example.com'
	EMAIL_HOST_PASSWORD: 'A_RANDOM_EMAIL_PASSWORD'
	EMAIL_PORT: 25
	
	DATABASES:
	    default:
	        'ENGINE': 'django.db.backends.postgresql_psycopg2'
	        'NAME': 'postgres'
	        'USER': 'postgres'
	        'PASSWORD': 'DB Password'
	        'HOST': 'db-master'
	        'PORT': '5432'
	    slave:
	        'ENGINE': 'django.db.backends.postgresql_psycopg2'
	        'NAME': 'postgres'
	        'USER': 'postgres'
	        'PASSWORD': 'DB Password'
	        'HOST': 'db-slave'
	        'PORT': '5432'
	
	TEMPLATES : [
	    {
	        'BACKEND': 'django.template.backends.django.DjangoTemplates',
	        'DIRS': ['/root/password_manager_server/templates'],
	        'APP_DIRS': True,
	        'OPTIONS': {
	            'context_processors': [
	                'django.template.context_processors.debug',
	                'django.template.context_processors.request',
	                'django.contrib.auth.context_processors.auth',
	                'django.contrib.messages.context_processors.messages',
	            ],
	        },
	    },
	]

5) Change Domainname and Server Port according to your whishes.

6) Leave Redis Location as is

7) Click Launch

### Start new PSONO Client Stack

1) Go to "Catalog -> 'Catalog name' -> PSONO Client View Details"

2) Change Server Port according to your whishes.

3) Specify the path to the config.json e.g.:

    /opt/docker/production-psono-client/config.json

It should look like this:

	{
		"backend_servers": [{
			"title": "My Server Title",
			"url": "URL_OF_THE_SERVER_SPECIFIED_IN_SERVER_CONFIG_AS_HOST_URL"
		}],
		"base_url": "URL_OF_THE_CLIENT",
		"allow_custom_server": true
	}

4) Click Launch
		

### Create your mail accounts
Don't forget to adapt `MAIL_USER` and `MAIL_PASS` to your needs

    cd /opt/docker/develop-psono-mail/
    touch ./config/postfix-accounts.cf
    docker run --rm \
      -e MAIL_USER=user1@domain.tld \
      -e MAIL_PASS=mypassword \
      -ti tvial/docker-mailserver:latest \
      /bin/sh -c 'echo "$MAIL_USER|$(doveadm pw -s SHA512-CRYPT -u $MAIL_USER -p $MAIL_PASS)"' >> config/postfix-accounts.cf


### Generate DKIM keys

    docker run --rm \
      -v "$(pwd)/config":/tmp/docker-mailserver \
      -ti tvial/docker-mailserver:latest generate-dkim-config
      
Now the keys are generated, you can configure your DNS server by just
pasting the content of `config/opendkim/keys/domain.tld/mail.txt` in your
`domain.tld.hosts` zone.


